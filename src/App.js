import './App.css';
import UseState from './components/UseState';
import UseEffect from './components/UseEffect';
import UseRef from './components/UseRef';
import UseCallback from './components/UseCallback';
import UseMemo from './components/UseMemo'
import {useCallback,useState} from 'react'
import UseReducer from './components/UseReducer';


function App() {

  const [count, setCount] = useState(0);
  const [todos, setTodos] = useState([]);

  const increment = () => {
    setCount((c) => c + 1);
  };
  const addTodo = useCallback(() => {
    setTodos((t) => [...t, "New Todo"]);
  }, []);

  return (
    <>
    <p>UseState Hook</p>
      <UseState /><hr /><hr />
    <p>UseEffect Hook</p>
      <UseEffect /><hr /><hr />
    <p>UseRef Hook</p>
      <UseRef /><hr /><hr />
    <p>UseCallback Hook</p>
      <UseCallback todos={todos} addTodo={addTodo} />
      <hr />
      <div>
        Count: {count}
        <button onClick={increment}>+</button>
      </div>
      <hr /><hr />

      <p>UseMemo Hook</p>
        <UseMemo /><hr /><hr />
      <p>UseReducer Hook</p>
        <UseReducer />
    </>
  );
}

export default App;
